import { type IGlassesData, GLASSES_TYPES } from '@/types/glasses'

export const getGlassesAPI = async (
  queryParams: string,
  glassesType: GLASSES_TYPES,
  pageNumber: number
): Promise<IGlassesData> => {
  const url = `https://staging-api.bloobloom.com/user/v1/sales_channels/website/collections/${glassesType}/glasses?sort[type]=collection_relations_position&sort[order]=asc&filters[lens_variant_prescriptions][]=fashion&filters[lens_variant_types][]=classic&filters[frame_variant_home_trial_available]=false&page[limit]=12&page[number]=${pageNumber}&${queryParams}`

  try {
    const response = await fetch(url)
    if (!response.ok) {
      throw new Error('Connection error, please check your internet connection')
    }
    const products = await response.json()
    return products
  } catch (error) {
    console.error('Error fetching data:', error)
    throw error
  }
}
