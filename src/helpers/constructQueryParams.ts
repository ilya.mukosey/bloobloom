import { type ActiveFilter, FILTERS_TYPE } from '@/types/glasses'

export const constructQueryParams = (filters: ActiveFilter[]) => {
  const queryArray = filters.map((filter) => {
    if (filter.type === FILTERS_TYPE.COLOUR) {
      return `filters[glass_variant_frame_variant_colour_tag_configuration_names][]=${filter.name}`
    }
    return `filters[glass_variant_frame_variant_frame_tag_configuration_names][]=${filter.name}`
  })

  return queryArray.join('&')
}
