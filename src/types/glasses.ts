interface Media {
  id: number
  medium_type: string
  mime_type: string
  file_location: string
  file_name: string
  original_file_name: string
  size: number
  url: string
  position: number
}

interface Colour {
  id: number
  name: string
  media: Media[]
}

interface FrameVariant {
  id: number
  name: string
  configuration_name: string
  barcode: string | null
  harmonized_system_code: string | null
  stock_keeping_unit: string | null
  status: string
  colour: Colour
}

interface GlassVariant {
  id: number
  barcode: string | null
  harmonized_system_code: string | null
  stock_keeping_unit: string | null
  inventory: boolean
  home_trial_available: boolean
  price: number
  default_glass_variant: boolean
  frame_variant: FrameVariant
  media: Media[]
}

interface CostBreakdown {
  id: number
  materials: number
  labour: number
  transport: number
  taxes: number
  bloobloom_price: number
  retail_price: number
}

export interface Glass {
  id: number
  name: string
  configuration_name: string
  default_collection_name: string | null
  ar_assets: any[] // You can replace 'any' with a specific type if needed
  cost_breakdown: CostBreakdown
  glass_variants: GlassVariant[]
}
export interface Meta {
  total_count: number
}

export interface IGlassesData {
  glasses: Glass[]
  meta: Meta
}
export enum COLOUR_FILTERS {
  BLACK = 'black',
  TORTOISE = 'tortoise',
  COLOURED = 'coloured',
  CRYSTAL = 'crystal',
  DARK = 'dark',
  BRIGHT = 'bright'
}
export enum SHAPE_FILTERS {
  SQUARE = 'square',
  RECTANGLE = 'rectangle',
  ROUND = 'round',
  CAT_EYE = 'cat-eye'
}
export enum GLASSES_TYPES {
  SPECT_FOR_WOMEN = 'spectacles-women',
  SPECT_FOR_MEN = 'spectacles-men',
  SUN_FOR_WOMEN = 'sunglasses-women',
  SUN_FOR_MEN = 'sunglasses-men'
}
export enum FILTERS_TYPE {
  COLOUR,
  SHAPE
}
export interface ActiveFilter {
  type: FILTERS_TYPE
  label: string
  name: SHAPE_FILTERS | COLOUR_FILTERS
}
export type AddFilterFunction = (filter: ActiveFilter) => void
